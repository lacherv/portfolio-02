/** ----- IMPORT SCSS ----- */
import "./intro.scss";
/** ----- IMPORT JS Library  ----- */
import { init } from 'ityped';
import { useEffect, useRef } from "react";


function Intro() {

    const textRef = useRef();

    const opsRef = useRef();

    useEffect(() => {
        init(textRef.current, {
            showCursor: true,
            backDelay: 1500,
            backSpeed: 50,
            strings: ["React", "Front-End"]
        })
    }, []);

    useEffect(() => {
        init(opsRef.current, {
            showCursor: false,
            backDelay: 3500,
            backSpeed: 150,
            strings: ["Linux", "IT"]
        })
    }, []);

    return (
        <div className="intro" id="intro">
            <div className="left">
                <div className="imgContainer">
                    <img src="assets/man.png" alt="me" />
                </div>
            </div>
            <div className="right">
                <div className="wrapper">
                    <h2>Hi There, I'm</h2>
                    <h1>Christian Awelakoue</h1>
                    <h3><span ref={opsRef}></span> SysAdmin</h3> 
                    <h3>Aspiring <span ref={textRef}></span> Developer</h3>
                </div>
                <a href="#portfolio">
                    <img src="assets/down.png" alt="" />
                </a>
            </div>
        </div>
    )
}

export default Intro;
