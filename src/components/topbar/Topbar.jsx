/** ----- IMPORT SCSS ----- */
import "./topbar.scss";
/** ----- IMPORT MATERIAL UI ----- */
import { Phone, Mail } from "@material-ui/icons";

function Topbar({ menuOpen, setMenuOpen }) {
    return (
        <div className={"topbar " + (menuOpen && "active")}>
            <div className="wrapper">
                <div className="left">
                    <a href="#intro" className="logo">OpenMinded.</a>
                    <div className="itemContainer">
                        <Phone className="icon" />
                        <span>+27 78 666 13 47</span>
                    </div>
                    <div className="itemContainer">
                        <Mail className="icon" />
                        <span>awelakoue(at)gmail(dot)com</span>
                    </div>
                </div>
                <div className="right">
                    <div className="hamburger" onClick={() => setMenuOpen(!menuOpen)}>
                        <span className="line1"></span>
                        <span className="line2"></span>
                        <span className="line3"></span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Topbar;
